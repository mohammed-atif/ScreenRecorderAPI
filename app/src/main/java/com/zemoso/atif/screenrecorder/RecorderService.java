package com.zemoso.atif.screenrecorder;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.opengl.Visibility;
import android.os.Environment;
import android.os.IBinder;
import android.provider.MediaStore;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

public class RecorderService extends Service {
    public static boolean status = false;
    private final int SERVICE_ID=7;
    Intent playIntent;
    Intent stopIntent;
    public static boolean playStatus = false;
    public static Class CURRENT_CLASS;

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this, "Service starting", Toast.LENGTH_LONG).show();
        status = true;

        //Setting up notification
        Notification noti = new Notification.Builder(this)
                .setContentTitle("Notification title")
                .setContentText("Notification content")
                .setSmallIcon(R.drawable.ic_media_play)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setOngoing(true)
                .build();// Again, THIS is the important line

        //Setting up remote view for custom notifications
        RemoteViews notificationView = new RemoteViews(getPackageName(),
                R.layout.notification_layout);

        //Creating intents for play pause and stop
        playIntent = new Intent(this, playService.class);
        stopIntent = new Intent(this, stopService.class);
        Intent mainIntent = new Intent(this, CURRENT_CLASS);

        //Creating pending intents for play pause and stop
        PendingIntent pendingPlayIntent = PendingIntent.getService(this, 0,
                playIntent, 0);
        PendingIntent pendingStopIntent = PendingIntent.getService(this, 0,
                stopIntent, 0);

        PendingIntent pendingMain = PendingIntent.getActivity(this, 0, mainIntent, 0);

        //adding on click listeners
        notificationView.setOnClickPendingIntent(R.id.PlayNotification,
                pendingPlayIntent);
        notificationView.setOnClickPendingIntent(R.id.StopNotification,
                pendingStopIntent);
        notificationView.setOnClickPendingIntent(R.id.defaultView,
                pendingMain);
        noti.contentView = notificationView;
        noti.contentIntent = pendingPlayIntent;



        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(SERVICE_ID, noti);
        return START_STICKY;
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        status = false;
        stopService(playIntent);
        NotificationManager manager=(NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.cancel(7);
        Toast.makeText(this, "Service stopped", Toast.LENGTH_LONG).show();

    }

}


