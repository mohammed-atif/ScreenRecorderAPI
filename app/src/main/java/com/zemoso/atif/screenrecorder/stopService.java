package com.zemoso.atif.screenrecorder;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class stopService extends Service {
    public stopService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("Service", "In Play ");
        if(RecorderService.playStatus) {
            Intent playIntent = new Intent(this, RecordControllerService.class);
            stopService(playIntent);
        }else Toast.makeText(this, "Recorder already stopped", Toast.LENGTH_LONG).show();
        return super.onStartCommand(intent, flags, startId);
    }
}
